package edu.ssu_matmod_main_Package.SSU_tasks_Package;

import edu.ssu_matmod_main_Package.Usefull_submodules.Simple_Timer;

import java.util.Scanner;

import static edu.ssu_matmod_main_Package.Static_Data.CURSOR;

//класс от которого наследуются другие классы с заданиями
public abstract class Task {
    protected Scanner sc = new Scanner(System.in);

    public  abstract void run(String mode) throws InterruptedException;
    public abstract String getName();
    public abstract String getParam(int mode);

    protected void freeze(Task task, String param, long time, String timeformat){
        System.out.printf("Задание:%s\tПараметр:%s\tСтатус:Завершено\tВремя выполнения:%s %s\n", task.getName(), param, time, timeformat);
    }

}
