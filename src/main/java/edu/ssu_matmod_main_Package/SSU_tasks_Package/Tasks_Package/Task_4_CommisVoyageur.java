package edu.ssu_matmod_main_Package.SSU_tasks_Package.Tasks_Package;

import edu.ssu_matmod_main_Package.SSU_tasks_Package.Task;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Task_4_CommisVoyageur extends Task {
    private static final String name = "Задача коммивояжера";
    private static final String param1 = "Полный перебор";
    private static final String param2 = "Муравьиный алгоритм";

    @Override
    public void run(String mode) throws InterruptedException {

    }

    @Override
    public String getName(){ return name; }

    @Override
    public  String getParam(int mode) {
        String temp = null;
        switch (mode) {
            case 0: temp = param1;break;
            case 1: temp = param2;break;
            default: break; }
        return temp;
    }



    private static class State{
        int cityNum;
        public int nextIndex;
        boolean isStartPoint;
        State prev;

        State(State prev,int city){
            this.prev=prev;
            cityNum=city;
            isStartPoint=false;
        }

        State(State prev,int city,boolean start){
            this.prev=prev;
            cityNum=city;
            isStartPoint=start;
        }

        int calculateLength(Graph graph){
            State current=this;
            int sum=0;

            while(current.prev!=null){
                sum+=graph.getEdge(current.prev.cityNum,current.cityNum);
                current=current.prev;
            }
            return sum;
        }

        @Override
        public String toString(){
            if(prev==null)return Integer.toString(cityNum);
            else{
                return prev.toString()+" "+Integer.toString(cityNum);
            }
        }
    }


    private static class Graph {
        private int count;
        private int[][] matrix;
        private boolean[] marks;

        Graph(int count){
            this.count=count;
            matrix=new int[count][count];
            marks=new boolean[count];
        }

        void setEdge(int a, int b, int weight){
            matrix[a][b]=weight;
            matrix[b][a]=weight;
        }

        int getEdge(int a, int b){
            return matrix[a][b];
        }

        public boolean hasEdge(int a,int b){
            return matrix[a][b]!=0;
        }

        static Graph load(File file) throws IOException {
            Scanner sc = new Scanner(file);

            Graph graph=new Graph(sc.nextInt());

            while(sc.hasNext()){
                int a=sc.nextInt();
                int b=sc.nextInt();
                int weight=sc.nextInt();
                graph.setEdge(a,b,weight);
            }

            return graph;
        }

        public static Graph load(String filename) throws IOException { return load(new File(filename)); }

        public boolean enter(int pos){
            if(marks[pos]){ return false;
            }else{
                marks[pos]=true;
                return true;
            }
        }

        public void leave(int pos){ marks[pos]=false; }
        public int getCount(){ return count; }

        public boolean allVisited(){
            for (boolean mark : marks) {
                if (!mark) return false;
            }
            return true;
        }
    }



}
