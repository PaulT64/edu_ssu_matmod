package edu.ssu_matmod_main_Package.SSU_tasks_Package.Tasks_Package;

import edu.ssu_matmod_main_Package.SSU_tasks_Package.Task;
import edu.ssu_matmod_main_Package.Usefull_submodules.Simple_Timer;

import static edu.ssu_matmod_main_Package.Static_Data.*;

public class Task_3_Fibonacci_Number extends Task {
    private static final String name = "N-ый член ряда Фибоначчи";
    private static final String param1 = "Цикл";
    private static final String param2 = "Рекурсия";

    @Override
    public void run(String mode){
        int n;
        switch (mode){
            case param1:
                System.out.print("Номер искомого члена ряда:");
                n = sc.nextInt();
                Simple_Timer.run(START);
                cycle(n);
                Simple_Timer.run(STOP);
                super.freeze(this, this.getParam(0), Simple_Timer.getResult(milliseconds), milliseconds);
                Simple_Timer.clear();
                break;
            case param2:
                System.out.print("Номер искомого члена ряда:");
                n = sc.nextInt();
                Simple_Timer.run(START);
                recursion(n);
                Simple_Timer.run(STOP);
                super.freeze(this, this.getParam(1), Simple_Timer.getResult(milliseconds), milliseconds);
                Simple_Timer.clear();
                break;
            default:
                System.out.print("Такого параметра не существует");
                break;
        }
    }

    private void cycle(int n){ System.out.printf("Результат: %s\n",cycleMain(n)); }
    private long cycleMain(int n){
        long n0 = 1;
        long n1 = 1;
        long n2 = 0;
        for(int i = 3; i <= n; i++){
            n2=n0+n1;
            n0=n1;
            n1=n2;
        }
        return n2;
    }

    private void recursion(int n) { System.out.printf("Результат: %s\n",recursionMain(n)); }
    private long recursionMain(int n){
            if (n == 0){ return 0; }
            if (n == 1){ return 1; }
            else{ return recursionMain(n - 1) + recursionMain(n - 2); }
    }

    @Override
    public  String getParam(int mode) {
        String temp = null;
        switch (mode) {
            case 0: temp = param1;break;
            case 1: temp = param2;break;
            default: break; }
        return temp;
    }

    @Override
    public String getName(){ return name; }

}
