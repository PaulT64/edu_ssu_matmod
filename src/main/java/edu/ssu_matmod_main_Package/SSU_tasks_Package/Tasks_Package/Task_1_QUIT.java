package edu.ssu_matmod_main_Package.SSU_tasks_Package.Tasks_Package;

import edu.ssu_matmod_main_Package.SSU_tasks_Package.Task;

public class Task_1_QUIT extends Task {
    private static final String name = "Выход";
    private static String param1 = name;
    private static String param2;

    @Override
    public void run(String mode){
        if (mode.equals(param1)){
            System.exit(0);
        }
    }

    @Override
    public  String getParam(int mode) {
        String temp = null;
        switch (mode) {
            case 0: temp = param1;break;
            case 1: temp = param2;break;
            default: break; }
        return temp;
    }

    @Override
    public String getName(){ return name; }
}
