package edu.ssu_matmod_main_Package.SSU_tasks_Package.Tasks_Package;

import edu.ssu_matmod_main_Package.SSU_tasks_Package.Task;
import edu.ssu_matmod_main_Package.Usefull_submodules.Simple_Timer;

import static edu.ssu_matmod_main_Package.Static_Data.*;
import static java.lang.Thread.sleep;

public class Task_2_TEST extends Task {
    private static final String name = "Тест системы";
    private static String param1 = "Запустить тест";
    private static String result;

    @Override
    public void run(String mode) throws InterruptedException {
        if (mode.equals(param1)) {
            Simple_Timer.run(START);
            while (Simple_Timer.current_time(FROM_START, seconds) != 5) {
                sleep(SEC);
                System.out.print("\r");
                result = String.format("Testing. Current time: %s %s", Simple_Timer.current_time(FROM_START, seconds), seconds);
                System.out.printf("%s", result);
            }
            System.out.printf("\r%s\n", result);
            Simple_Timer.run(STOP);
            super.freeze(this, this.getParam(0), Simple_Timer.getResult(milliseconds), milliseconds);
        }
    }

    @Override
    public  String getParam(int mode) {
        String temp = null;
        switch (mode) {
            case 0: temp = param1;break;
            default: break; }
        return temp;
    }

    @Override
    public String getName() { return name; }

}
