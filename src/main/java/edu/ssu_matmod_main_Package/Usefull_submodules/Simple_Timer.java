package edu.ssu_matmod_main_Package.Usefull_submodules;

import static java.lang.Thread.sleep;

public class Simple_Timer {
    private static long instance_start_time;
    private static long time_result;
    private static long start_time;
    private static long stop_time;
    private static long current_time;

    public Simple_Timer(){ instance_start_time = System.currentTimeMillis();}

    public static void run(String mode){
        switch (mode){
            case "start":
                start_time = System.currentTimeMillis();
                break;

            case "stop" :
                stop_time = System.currentTimeMillis();
                time_result = stop_time - start_time;
                break;

            default: break;
        }
    }

    public static void clear(){
        start_time = 0;
        stop_time = 0;
        time_result = 0;
        current_time = 0;
    }

    public static long getResult(String mode){
        time_result = change_by_mode(mode, time_result);
        return time_result;
    }

    public static long current_time(String mode, String time_mode){
        long temp_timestamp = System.currentTimeMillis();
        switch (mode){
            case "system":
                current_time = temp_timestamp - instance_start_time;
                break;
            case "from start":
                current_time = temp_timestamp - start_time;
                break;
            default:
                break;
        }
        current_time = change_by_mode(time_mode, current_time);
        return current_time;
    }

    private static long change_by_mode(String mode, long variable){
        long temp_variable = 0;
        switch (mode) {
            case "ms": temp_variable = variable; break;
            case "s" : temp_variable = variable / 1_000; break;
            case "m" : temp_variable = (variable / 1_000) % 60; break;
            case "h" : temp_variable = ((variable / 1_000) % 60) % 60; break;
            default: break;
        }
        variable = temp_variable; return variable;
    }

}