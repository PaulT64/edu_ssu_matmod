package edu.ssu_matmod_main_Package.System_core_Package.UI_core_Package.Interfaces_Package;

import edu.ssu_matmod_main_Package.SSU_tasks_Package.Task;
import edu.ssu_matmod_main_Package.System_core_Package.UI_core_Package.User_Interface;

import java.util.ArrayList;
import java.util.Scanner;

import static edu.ssu_matmod_main_Package.Static_Data.CURSOR;
import static edu.ssu_matmod_main_Package.Static_Data.TASK_ARRAY;

public class Task_Console extends User_Interface {
    private ArrayList<String> task_parameters = new ArrayList<>(3);
    private Scanner sc = new Scanner(System.in);

    public void run(int h) throws InterruptedException {
        Task task = TASK_ARRAY.get(h);
        task_parameters.add(task.getName());
        task_parameters.add("Назад");
        if (task.getParam(0) != null) task_parameters.add(task.getParam(0));
        if (task.getParam(1) != null) task_parameters.add(task.getParam(1));

        String input;
        while (true){
            System.out.printf("Задание: %s\n", task.getName());
            for (int i = 1; i< task_parameters.size(); i++){
                System.out.printf("%s: %s\n",i , task_parameters.get(i));
            }
            System.out.print(CURSOR);
            input = sc.nextLine().toLowerCase();

            if (input.matches("\\d")) {
                int k = Integer.parseInt(input);
                if (k <= task_parameters.size()-1 && k != 1){
                    task.run(task_parameters.get(k));
                    sc.nextLine();
                }else {break;}
            }
        }
    }
}
