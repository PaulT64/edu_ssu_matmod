package edu.ssu_matmod_main_Package.System_core_Package.UI_core_Package.Interfaces_Package;

import edu.ssu_matmod_main_Package.System_core_Package.UI_core_Package.User_Interface;
import java.util.Scanner;

import static edu.ssu_matmod_main_Package.Static_Data.CURSOR;
import static edu.ssu_matmod_main_Package.Static_Data.TASK_ARRAY;

public class Main_Console extends User_Interface {
    private Scanner scanner = new Scanner(System.in);

    public Main_Console(){}
    public void run(int h) throws InterruptedException {
        String input;
        String temp;
        while (true){
            System.out.println("Главное меню. Выбери действие: \n");
            for(int i=0; i<TASK_ARRAY.size(); i++){
                temp = String.format("%s: %s", i, TASK_ARRAY.get(i).getName());
                System.out.println(temp);
            }
            System.out.print(CURSOR); input = scanner.nextLine().toLowerCase();
            if (input.matches("\\d")){
                clean();
                int k = Integer.parseInt(input);
                if( k <= TASK_ARRAY.size()){
                    Task_Console task_console = new Task_Console();
                    task_console.run(k);
                    clean();
                }
            }else { clean(); }

        }
    }

    private void clean(){
        for (int i=0; i<50; i++){ System.out.println(); }
    }
}
