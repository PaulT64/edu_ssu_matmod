package edu.ssu_matmod_main_Package.System_core_Package;

import edu.ssu_matmod_main_Package.SSU_tasks_Package.Task;
import edu.ssu_matmod_main_Package.System_core_Package.UI_core_Package.User_Interface;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import static edu.ssu_matmod_main_Package.Static_Data.*;


public class Core {

    private ArrayList<Task> tasks_array = new ArrayList<>(); //массив выполненных заданий
    private ArrayList<String> task_name_array = new ArrayList<>();

    public Core(){}

    public void run() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, InterruptedException {

        String class_name;
        File folder = new File(PATH_TO_TASK);
        File[] list_of_files = folder.listFiles();

        assert list_of_files != null;
        for (File file_name : list_of_files) {
            if (file_name.isFile()) {
                class_name = file_name.getName().substring(0, file_name.getName().indexOf("."));
                task_name_array.add(file_name.getName().substring(file_name.getName().indexOf("_")+1, file_name.getName().indexOf(".")));
                tasks_array.add((Task) Class.forName(TASK_REFERENCE + class_name).getConstructor().newInstance());
            }
        }

        //сохраняем массивы в статической памяти программы (видны из любой области программы)
        TASK_ARRAY = tasks_array;
        TASK_NAME_ARRAY = task_name_array;
        MAIN_CONSOLE.run(0);

    }
}
