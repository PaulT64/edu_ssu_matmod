package edu.ssu_matmod_main_Package;

import edu.ssu_matmod_main_Package.SSU_tasks_Package.Task;
import edu.ssu_matmod_main_Package.System_core_Package.UI_core_Package.Interfaces_Package.Main_Console;
import edu.ssu_matmod_main_Package.System_core_Package.UI_core_Package.User_Interface;
import edu.ssu_matmod_main_Package.Usefull_submodules.Simple_Timer;

import java.util.ArrayList;

public class Static_Data {

    public static final String PATH_TO_TASK = "./src/main/java/edu/ssu_matmod_main_Package/SSU_tasks_Package/Tasks_Package/";
    public static final String TASK_REFERENCE = "edu.ssu_matmod_main_Package.SSU_tasks_Package.Tasks_Package.";
    public static ArrayList<Task> TASK_ARRAY = new ArrayList<>();  //массив выполненных заданий
    public static ArrayList<String> TASK_NAME_ARRAY = new ArrayList<>(); //массив имен файлов классов выполненных заданий

    public static final String SYSTEM        = "system"     ;
    public static final String FROM_START    = "from start" ;
    public static final String START         = "start"      ;
    public static final String STOP          = "stop"       ;
    public static final String CURSOR        = "/: "        ;


    public static final String milliseconds = "ms";
    public static final long SEC  = 1_000    ; public static final String seconds = "s";
    public static final long MIN  = 60 * SEC ; public static final String minutes = "m";
    public static final long HOUR = 60 * MIN ; public static final String hours   = "h";
    public static final long DAY  = 24 * HOUR; public static final String days    = "d";


    public static final Simple_Timer GLOBAL_TIMER   = new Simple_Timer();
    public static final User_Interface MAIN_CONSOLE = new Main_Console();
}
