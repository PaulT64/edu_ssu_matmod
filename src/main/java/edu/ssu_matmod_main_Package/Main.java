package edu.ssu_matmod_main_Package;

import edu.ssu_matmod_main_Package.System_core_Package.Core;

import java.lang.reflect.InvocationTargetException;

import static edu.ssu_matmod_main_Package.Static_Data.*;

public class Main {
    public static void main(String[] args){
        try {
            Core core = new Core();
            core.run();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
